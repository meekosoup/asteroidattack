using UnityEngine;

[CreateAssetMenu(fileName = "PlayerData", menuName = "AsteroidAttack/PlayerData", order = 51)]
public class PlayerData : ScriptableObject
{
    public bool paused;
    public float ambienceVol = 1f;
    public float musicVol = 1f;
    public float sfxVol = 1f;

    public float AmbienceVol
    {
        get
        {
            ambienceVol = PlayerPrefs.GetFloat("AmbienceVol");
            return ambienceVol;
        }
        set
        {
            ambienceVol = value;
            PlayerPrefs.SetFloat("AmbienceVol", ambienceVol);
            PlayerPrefs.Save();
        }
    }

    public float MusicVol
    {
        get
        {
            musicVol = PlayerPrefs.GetFloat("MusicVol");
            return musicVol;
        }
        set
        {
            musicVol = value;
            PlayerPrefs.SetFloat("MusicVol", musicVol);
            PlayerPrefs.Save();
        }
    }

    public float SfxVol
    {
        get
        {
            sfxVol = PlayerPrefs.GetFloat("SfxVol");
            return ambienceVol;
        }
        set
        {
            sfxVol = value;
            PlayerPrefs.SetFloat("SfxVol", sfxVol);
            PlayerPrefs.Save();
        }
    }
    
    
    public bool musicMuted;

    public bool MusicMuted
    {
        get
        {
            musicMuted = PlayerPrefs.GetInt("MusicMuted") > 0;
            return musicMuted;
        }
        set
        {
            musicMuted = value;
            PlayerPrefs.SetInt("MusicMuted", musicMuted ? 1 : 0);
            PlayerPrefs.Save();
        }
    }

    public bool sfxMuted;

    public bool SfxMuted
    {
        get
        {
            musicMuted = PlayerPrefs.GetInt("SfxMuted") > 0;
            return musicMuted;
        }
        set
        {
            sfxMuted = value;
            PlayerPrefs.SetInt("SfxMuted", sfxMuted ? 1 : 0);
            PlayerPrefs.Save();
        }
    }

    public bool ambienceMuted;
    public bool AmbienceMuted
    {
        get
        {
            ambienceMuted = PlayerPrefs.GetInt("AmbienceMuted") > 0;
            return musicMuted;
        }
        set
        {
            ambienceMuted = value;
            PlayerPrefs.SetInt("AmbienceMuted", ambienceMuted ? 1 : 0);
            PlayerPrefs.Save();
        }
    }
    [SerializeField] private int health = 3;

    public int Health
    {
        get => health;
        set => health = value;
    }
}
