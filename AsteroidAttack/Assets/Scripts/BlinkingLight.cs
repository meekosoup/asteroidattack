using System;
using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;
using Random = UnityEngine.Random;

public class BlinkingLight : MonoBehaviour
{
    public enum StrobePattern
    {
        Normal,
        Flicker,
    }

    [SerializeField] private PlayerData playerData;
    public bool blinkingOn = true;
    public StrobePattern strobePattern;

    [Header("Flicker Pattern"), Range(0f, 1f)]
    public float flickerDropIntensity = 0.5f;

    public float flockerTimeMin = 1;
    public float flockerTimeMax = 3;
    
    [Range(-0.99f, 2f)]
    public float restoreIntensitySpeed = 0f;

    private Light2D _light;
    private float _originalIntensity;
    private float _u; // used to determine where we are in the strobe cycle
    private float _flickerClock;
    private float strobeCycle = 1;

    private void Awake()
    {
        _light = GetComponent<Light2D>();
        _originalIntensity = _light.intensity;
    }

    private void Update()
    {
        if (blinkingOn && !playerData.paused)
        {
            _u = Time.time;
            _light.intensity = StrobeCycleValue(_u);
        }
    }

    private float StrobeCycleValue(float time)
    {
        if (strobeCycle < 0)
            strobeCycle = 0;
        
        switch (strobePattern)
        {
            case StrobePattern.Normal:
                strobeCycle = Mathf.Abs(Mathf.Cos(time) * _originalIntensity);
                break;
            case StrobePattern.Flicker:
                _flickerClock -= Time.deltaTime;
                
                if (_flickerClock <= 0)
                {
                    strobeCycle = _originalIntensity * flickerDropIntensity; // drop strobe intensity suddenly
                    _flickerClock = Random.Range(flockerTimeMin, flockerTimeMax); // reset timer to random value
                    // Debug.Log($"flicker clock reset! strobeCycle = {strobeCycle.ToString()}");
                }
                else
                {
                    if (strobeCycle < _originalIntensity)
                    {
                        strobeCycle += Time.deltaTime * (1 + restoreIntensitySpeed);
                        if (strobeCycle > _originalIntensity)
                            strobeCycle = _originalIntensity;
                    }
                }
                
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
        
        return strobeCycle;
    }
}
