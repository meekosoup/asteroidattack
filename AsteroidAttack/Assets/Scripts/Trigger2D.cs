using UnityEngine;
using UnityEngine.Events;

public class Trigger2D : MonoBehaviour
{
    public UnityEvent triggerEvent;
    public string targetTag;

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag(targetTag))
        {
            triggerEvent?.Invoke();
        }
    }
}
