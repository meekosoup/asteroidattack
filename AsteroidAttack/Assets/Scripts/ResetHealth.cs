using System;
using UnityEngine;

public class ResetHealth : MonoBehaviour
{
    public PlayerData playerData;
    public int health = 3;

    private void OnValidate()
    {
        if (health <= 0)
        {
            health = 1;
        }
    }

    private void Awake()
    {
        playerData.Health = health;
    }
}
