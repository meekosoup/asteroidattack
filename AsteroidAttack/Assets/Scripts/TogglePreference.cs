using UnityEngine;
using UnityEngine.UI;

public class TogglePreference : MonoBehaviour
{
    public string loadData;
    private Toggle _toggle;

    private void OnEnable()
    {
        _toggle = GetComponent<Toggle>();
        _toggle.isOn = PlayerPrefs.GetInt(loadData) > 0;
    }
}
