using UnityEngine;

public class UIManager : MonoBehaviour
{
    public static UIManager Instance;
    [SerializeField] private PlayerData playerData;
    [SerializeField] private GameObject pauseMenu;

    private void Awake()
    {
        if (Instance != null && Instance != this)
            Destroy(this.gameObject);
        else
            Instance = this;
        
        DontDestroyOnLoad(this.gameObject);
        ClosePauseMenu();
    }

    public void OpenPauseMenu()
    {
        playerData.paused = true;
        pauseMenu.SetActive(true);
        SceneUtility.Pause();
    }

    public void ClosePauseMenu()
    {
        SceneUtility.Unpause();
        playerData.paused = false;
        pauseMenu.SetActive(false);
    }
}
