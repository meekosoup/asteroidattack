using UnityEngine;
using UnityEngine.SceneManagement;

public class HealthTracker : MonoBehaviour
{
    public PlayerData playerData;
    public string gameOverSceneName = "TempGameOver";

    private void Update()
    {
        if (playerData.Health <= 0)
        {
            SceneManager.LoadScene(gameOverSceneName);
        }
    }
}
