using System;
using TMPro;
using UnityEngine;

public class HealthUI : MonoBehaviour
{
    public TMP_Text healthText;
    public PlayerData playerData;

    private void LateUpdate()
    {
        healthText.text = $"Health: {playerData.Health.ToString()}";
    }
}
