using UnityEngine;
using UnityEngine.Audio;

public class AudioManager : MonoBehaviour
{
    public static AudioManager Instance;
    public PlayerData playerData;
    public AudioSource musicSource;
    public AudioSource sfxSource;
    public AudioMixer masterMixer;

    private void Awake()
    {
        if (Instance != null && Instance != this)
            Destroy(this.gameObject);
        else
            Instance = this;
        
        DontDestroyOnLoad(this.gameObject);
        UpdateAudioSettings();
    }

    public void MuteAmbience(bool newValue)
    {
        playerData.AmbienceMuted = newValue;
        UpdateAudioSettings();
    }

    public void MuteMusic(bool newValue)
    {
        playerData.MusicMuted = newValue;
        UpdateAudioSettings();
    }

    public void MuteSfx(bool newValue)
    {
        playerData.SfxMuted = newValue;
        UpdateAudioSettings();
    }

    public void ChangeMusicVolume(float vol)
    {
        playerData.MusicVol = vol;
        UpdateAudioSettings();
    }

    public void ChangeSfxVolume(float vol)
    {
        playerData.SfxVol = vol;
        UpdateAudioSettings();
    }

    public void ChangeAmbienceVolume(float vol)
    {
        playerData.AmbienceVol = vol;
        UpdateAudioSettings();
    }

    private void UpdateAudioSettings()
    {
        var db = Mathf.Lerp(-80f, 20f, playerData.MusicVol);
        masterMixer.SetFloat("MusicVol", playerData.MusicMuted ? -80f : db);
        db = Mathf.Lerp(-80f, 20f, playerData.AmbienceVol);
        masterMixer.SetFloat("AmbienceVol", playerData.AmbienceMuted ? -80f : db);
        db = Mathf.Lerp(-80f, 20f, playerData.SfxVol);
        masterMixer.SetFloat("SfxVol", playerData.MusicMuted ? -80f : db);
    }
}
