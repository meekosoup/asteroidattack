using UnityEngine;
using UnityEngine.InputSystem;


[RequireComponent(typeof(Rigidbody2D))]
public class PlayerController : MonoBehaviour, IHealth
{
    [Header("Required")] [Tooltip("Stores all the data that we might want to save and reload.")]
    public PlayerData playerData;

    [Tooltip("Prefab of the object to be used as bullets.")]
    public GameObject projectilePrefab;

    [Header("Ship Properties")] [Tooltip("How long do bullets last in seconds.")]
    public float bulletLife = 5f;

    [Tooltip("Amount of force applied to the ship when the weapons are fired.")]
    public float weaponForce = 10f;

    [Tooltip("Maximum speed the player is allowed to reach. (Hard cap)")]
    public float maxSpeed = 10f;

    [Tooltip("When on, the player will gradually slow down without any player input.")]
    public bool useDrag = true;

    [Tooltip("How much to reduce speed by per frame. Smaller number = less drag, so slows down slower.")]
    [Range(0f, .05f)]
    public float drag = 0.1f;

    [Tooltip("Turn this off when you don't want the player to move.")]
    public bool movementOn = true;

    public int Health
    {
        get => playerData.Health;
        private set
        {
            playerData.Health = value;
            if (playerData.Health < 0)
                playerData.Health = 0;
        }
    }

    // Caching variables for optimization
    private Vector2 _aimPosition;
    private Vector3 _aimDirection = Vector3.zero;
    private float _aimAngle = 0f;
    private float _currentSpeed = 0f;
    private Vector2 _currentVelocity = Vector2.zero;
    private Rigidbody2D _rb;
    [SerializeField]
    private bool paused = false;

    private GameObject _projectile;


    private void Awake()
    {
        _rb = GetComponent<Rigidbody2D>();
    }

    private void Update()
    {
        // do nothing past this point if the game is paused
        if (playerData.paused)
            return;
        
        LimitSpeed();
        ApplyDrag();
    }

    private void LimitSpeed()
    {
        _currentSpeed = _rb.velocity.magnitude;

        if (_currentSpeed > maxSpeed)
        {
            _currentVelocity = _rb.velocity;
            _currentVelocity.x = Mathf.Clamp(_currentVelocity.x, -maxSpeed, maxSpeed);
            _currentVelocity.y = Mathf.Clamp(_currentVelocity.y, -maxSpeed, maxSpeed);
            _rb.velocity = _currentVelocity;
        }
    }

    private void ApplyDrag()
    {
        if (useDrag)
            _rb.velocity *= (1 - drag);
    }

    public void Aim(InputAction.CallbackContext context)
    {
        if (playerData.paused)
            return;
        
        _aimPosition = context.ReadValue<Vector2>();

        if (Camera.main != null)
            _aimDirection = _aimPosition - (Vector2)Camera.main.WorldToScreenPoint(transform.position);

        _aimAngle = Mathf.Atan2(_aimDirection.y, _aimDirection.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.AngleAxis(_aimAngle, Vector3.forward);
    }

    public void Fire(InputAction.CallbackContext context)
    {
        if (!context.performed)
            return;
        
        if (playerData.paused)
            return;

        if (projectilePrefab == null)
            return;

        _projectile = Instantiate(projectilePrefab, transform.position, transform.rotation);
        
        Destroy(_projectile, bulletLife);

        // Debug.Log($"Fire!");

        ApplyOppositeForce();
    }

    private void ApplyOppositeForce()
    {
        if (_rb == null || movementOn == false)
            return;

        var forceDirection = -MathHelpers.DegreeToVector2(_aimAngle);

        _rb.AddForce(forceDirection * Mathf.Clamp(weaponForce, 0f, maxSpeed), ForceMode2D.Impulse);
    }

    public void TakeDamage(int amount)
    {
        Health -= amount;
    }

    public void TogglePause(InputAction.CallbackContext context)
    {
        if (context.performed)
        {
            if (playerData.paused)
            {
                UIManager.Instance.ClosePauseMenu();
            }
            else
            {
                UIManager.Instance.OpenPauseMenu();
            }
        }
    }

}
