using UnityEngine;
using UnityEngine.UI;

public class SliderPreference : MonoBehaviour
{
    public string loadData;
    private Slider _slider;

    private void OnEnable()
    {
        _slider = GetComponent<Slider>();
        _slider.value = PlayerPrefs.GetFloat(loadData);
    }
}
