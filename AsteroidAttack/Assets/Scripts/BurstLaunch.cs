using DG.Tweening;
using UnityEngine;
using UnityEngine.Events;

public class BurstLaunch : MonoBehaviour
{
    public Transform target;
    public float duration = 3f;
    public Ease easing = Ease.Linear;
    public bool launchOnStart;
    public UnityEvent launchEvent;
    public UnityEvent landEvent;

    private void Start()
    {
        if (launchOnStart)
            Launch();
    }

    public void Launch()
    {
        launchEvent.Invoke();
        transform.DOMove(target.position, duration).SetEase(easing).OnComplete(() => Land());
    }

    private void Land()
    {
        landEvent.Invoke();
    }
    
    void OnDrawGizmosSelected()
    {
        if (target != null)
        {
            // Draws a blue line from this transform to the target
            Gizmos.color = Color.red;
            Gizmos.DrawLine(transform.position, target.position);
        }
    }
}
