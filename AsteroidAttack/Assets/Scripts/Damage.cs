using System;
using UnityEngine;


public class Damage : MonoBehaviour
{
    [SerializeField] private int damage = 1;

    private void OnTriggerEnter2D(Collider2D other)
    {
        IHealth health = other.gameObject.GetComponent<IHealth>();

        health?.TakeDamage(damage);
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        IHealth health = other.gameObject.GetComponent<IHealth>();

        health?.TakeDamage(damage);
    }
}